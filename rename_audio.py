import os
import pathlib
import shutil
from glob import glob

# 元ファイルディレクトリ
src_path = pathlib.Path('./combined_audio')

# 作業用ディレクトリ
processed_path = pathlib.Path('./processed_audio')

# 作業用ディレクトリが無い場合に元ファイルから複製
if not os.path.exists(processed_path):
    shutil.copytree(src_path, processed_path)

# 作業用ディレクトリ配列
processing_dir = list(processed_path.iterdir())

# ディレクトリ毎に処理
for processing in processing_dir:

    # gradeディレクトリ毎に処理
    for processing_file in processing.iterdir():

        if not processing_file.is_dir():

            # oldpath = pathlib.Path(processing_file)
            # oldpath.rename(pathlib.Path('./memo.mp3'))

            # print(processing_file.suffix)

            if processing_file.suffix in '.mp3':

                pathname = str(processing_file).split('/')
                gradePath = pathname[1]
                filename = processing_file.stem.split(' ')

                # print(gradePath)
                # print(filename)
                # print(filename[1])

                new = './processed_audio/' + gradePath + '/' + filename[1] + processing_file.suffix

                processing_file.rename(new)

                # print(processing_file.stem)
                # print(new)