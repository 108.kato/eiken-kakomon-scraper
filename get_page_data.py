from bs4 import BeautifulSoup
import urllib.request as req
import re
import urllib
import os
import time
from urllib.parse import urljoin
from urllib.parse import urlparse
from urllib.parse import urlsplit
import requests
import pandas as pd

import json



lists = [
    'https://www.eiken.or.jp/eiken/exam/grade_1/solutions.html'
    ,'https://www.eiken.or.jp/eiken/exam/grade_p1/solutions.html'
    ,'https://www.eiken.or.jp/eiken/exam/grade_2/solutions.html'
    ,'https://www.eiken.or.jp/eiken/exam/grade_p2/solutions.html'
    ,'https://www.eiken.or.jp/eiken/exam/grade_3/solutions.html'
    ,'https://www.eiken.or.jp/eiken/exam/grade_4/solutions.html'
    ,'https://www.eiken.or.jp/eiken/exam/grade_5/solutions.html'
    ,'https://www.eiken.or.jp/eiken/exam/grade_p1/solutions.html'
    ,'https://www.eiken.or.jp/eiken/exam/grade_p2/solutions.html'
]

base_url = 'https://www.eiken.or.jp'

for site in lists :
    html_text = requests.get( site ).content
    soup = BeautifulSoup(html_text, 'html.parser', from_encoding='utf-8')
    link = soup.body.find_all( "a" )
    url = urlparse( site )

    grade = url.path.split('/')[3]
    year = soup.body.find_all( "h4" )
    held = soup.body.find_all( "h4" )

    held_block = soup.body.find_all('ul', class_='list-link-01')

    print(held_block[0])

    print( grade )

    # print( year )

    download_data = {}
    data_arr = []

    for href in link :
        link_href = href.get('href')
        link_text = href.get_text()
        link_href = urllib.parse.urljoin(base_url, link_href)

        download_data_block = {}

        if '.pdf' in link_href or '.mp3' in link_href :

            if '問題冊子' in link_text or 'リスニング原稿' in link_text or 'リスニング音源' in link_text or '解答' in link_text :
                # print(link_text)

                download_data_block['name'] = link_text

                if '問題冊子':
                    download_data_block['type'] = '問題冊子'
                if 'リスニング原稿':
                    download_data_block['type'] = 'リスニング原稿'
                if 'リスニング音源':
                    download_data_block['type'] = 'リスニング音源'
                if '解答':
                    download_data_block['type'] = '解答'

                download_data_block['link'] = link_href

                data_arr.append(download_data_block)



    # print(data_arr)

    # dir_path = './data/' + grade
    # os.makedirs(dir_path, exist_ok=True)
    loop_count = 0
    for count, block in enumerate(year) :
        block_year = re.sub(r"\D", "", year[count].get_text().split(' ')[0])
        block_held = re.sub(r"\D", "", year[count].get_text().split(' ')[1])

        content_data = {
            grade: [
                {
                    "year": block_year,
                    "held": block_held,
                    "data": data_arr
                }
            ],
        }

        with open(grade + ".json", "w") as f:
            json.dump(content_data, f, indent=4)

    # time.sleep(30)